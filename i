mkfs.ext4 -F /dev/nvme0n1p2 &&
mkfs.fat -F32 /dev/nvme0n1p1 &&
mount /dev/nvme0n1p2 /mnt &&
mkdir /mnt/boot &&
mount /dev/nvme0n1p1 /mnt/boot &&
cp -R a/c/mirrorlist /etc/pacman.d/ &&
cp -R a/c/pacman.conf /etc/pacman.conf &&
pacstrap /mnt intel-ucode base base-devel efibootmgr dosfstools ttf-sazanami wget dbus-glib ffmpeg linux dhcpcd nano flameshot xfce4-settings pavucontrol git neofetch xorg-server xorg-xauth xorg-xinit xf86-input-libinput transmission-gtk gparted ttf-bitstream-vera pulseaudio pacman-contrib xfce4-panel xfce4-session xfce4-terminal xfdesktop xfwm4 gtk-engine-murrine leafpad thunar xfce4-whiskermenu-plugin &&
genfstab -U /mnt > /mnt/etc/fstab &&
echo en_CA.UTF-8 UTF-8 > /mnt/etc/locale.gen &&
echo arch > /mnt/etc/hostname &&
sudo mkdir /mnt/boot/loader/ &&
echo default arch > /mnt/boot/loader/loader.conf &&
echo NAutoVTs=3 >> /mnt/etc/systemd/logind.conf &&
echo DefaultTimeoutStartSec=5s >> /mnt/etc/systemd/system.conf &&
echo DefaultTimeoutStopSec=5s >> /mnt/etc/systemd/system.conf &&
echo DefaultTimeoutStartSec=5s >> /mnt/etc/systemd/user.conf &&
echo DefaultTimeoutStopSec=5s >> /mnt/etc/systemd/user.conf &&
echo Storage=none >> /mnt/etc/systemd/journald.conf &&
mv a/c/sudoers /mnt/etc/sudoers &&
mv a/c/pacman.conf /mnt/etc/pacman.conf &&
mv a/ii /mnt/ii.sh &&
arch-chroot /mnt useradd -m arch &&
arch-chroot /mnt su arch ii.sh &&
reboot
